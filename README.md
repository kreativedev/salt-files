## How to use?

Add the following to user data for AMI bootup:

### Worker Machine:

#!/bin/bash

wget https://gitlab.com/kreativedev/salt-files/raw/master/installsalt.sh && bash installsalt.sh

### Haproxy Balancer:

#!/bin/bash

wget https://gitlab.com/kreativedev/salt-files/raw/master/installhaproxy.sh && bash installhaproxy.sh

### Salt/Docker Master

#!/bin/bash

wget https://gitlab.com/kreativedev/salt-files/raw/master/installmaster.sh && bash installmaster.sh


#### Optional:

echo 'master: xx.xx.xx.xx' >> /etc/salt/minion

service salt-minion restart