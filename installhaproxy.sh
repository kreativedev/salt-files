#!/bin/bash
apt-get update && apt-get install -y wget && wget -O - https://repo.saltstack.com/apt/ubuntu/ubuntu14/latest/SALTSTACK-GPG-KEY.pub | sudo apt-key add - && echo "deb http://repo.saltstack.com/apt/ubuntu/ubuntu14/latest trusty main" >> /etc/apt/sources.list && apt-get update && apt-get install -y salt-minion && salt-call --local grains.append cluster dockerBalancer
cat <<EOT>> /etc/salt/minion
mine_functions:
  network.ip_addrs:
    interface: eth0
EOT